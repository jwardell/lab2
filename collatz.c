#include <stdio.h>

int get_start();
int next_collatz(int num);

int main()
{
    int num = get_start();
    int series = num;
    do  // Generate Collatz Series based on the user entered integer
    {
        printf(" %d,", series);
        series = next_collatz(series);
    } while(series != 1);
    printf(" 1\n");
}

int get_start() // Prompts the user to enter an integer and makes sure it is positive
{
    int num;
    do
    {
        printf("Enter a positive integer: ");
        scanf("%d", &num);
    } while (num < 1);
    return num;
} // end get_start()

int next_collatz(int num) //Given any integer, will return next entry in the Collatz Series
{
    if (num % 2 == 0)
    {
        return num / 2;
    }
    else
    {
        return (num * 3) + 1;
    }
} // end next_collatz

#include <stdio.h>

long get_cc()
{
    long cc;
    printf("Enter your credit card number: ");
    scanf("%ld", &cc);
    return cc;
}

char validate(long cc)
{
    int result;
    int sum = 0;
    int sumDoubles = 0;
    int digit;
    
    if (cc / 1000000000000000 != 0)
    {
        for( int i = 1; i <= 16; i++ )
        {
            result = cc % 10;
            digit = result;
            cc = cc / 10;
            if ( i % 2 == 0 )
            {
                result = result * 2;
                sumDoubles = sumDoubles + ( result % 10 ) + ( result / 10 );
            }
            else
            {
                sum = sum + result;
            }
        }
        if ( (sum + sumDoubles) % 10 == 0 )
        {
            switch(digit)
            {
                case 4:
                    return 'v';
                    break;
                case 5:
                    return 'm';
                    break;
                case 6:
                    return 'd';
                    break;
                default:
                    return 'i';
            }
        }
        else
        {
            return 'i';
        }
    }
    else if ( cc / 100000000000000 != 0 )
    {
        for( int i = 1; i <= 15; i++ )
        {
            result = cc % 10;
            digit = result;
            cc = cc / 10;
            if ( i % 2 == 0 )
            {
                result = result * 2;
                sumDoubles = sumDoubles + ( result % 10 ) + ( result / 10 );
            }
            else
            {
                sum = sum + result;
            }
        }
        if ( (sum + sumDoubles) % 10 == 0 )
        {
            switch(digit)
            {
                case 3:
                    return 'a';
                    break;
                default:
                    return 'i';
            }
        }
        else
        {
            return 'i';
        }
    }
    else
    {
        return 'i';
    }
}

int main()
{
    long cc = get_cc();
    char result = validate(cc);
    
    switch(result)
    {
        case 'a':
            printf("AMERICAN EXPRESS\n");
            break;
        case 'v':
                printf("VISA\n");
            break;
        case 'd':
            printf("DISCOVER\n");
            break;
        case 'm':
                printf("MASTERCARD\n");
            break;
        case 'i':
            printf("INVALID\n");
            break;
    }
}